include { determine_min_read_length; trim_reads; estimate_genome_size; correct_reads; downsample_reads } from './processes'

def find_genome_size(pair_id, mash_output) {
    m = mash_output =~ /Estimated genome size: (.+)/
    genome_size = Float.parseFloat(m[0][1]).toInteger()
    return [pair_id, genome_size]
}

def find_average_depth(pair_id, lighter_output){
  m = lighter_output =~  /.+Average coverage is ([0-9]+\.[0-9]+)\s.+/
  average_depth = Float.parseFloat(m[0][1])
  return [pair_id, average_depth]
}

workflow polish_reads {
  take: reads

  main:
    // get read lengths
    read_lengths = determine_min_read_length(reads)
    reads_and_lengths = reads.join(read_lengths)

    // trim reads
    trimmed_reads = trim_reads(reads_and_lengths, params.adapter_file)

    // estimate genome size
    mash_output = estimate_genome_size(reads)
    genome_sizes = mash_output.map { pair_id, file -> find_genome_size(pair_id, file.text) }
    reads_and_genome_sizes = trimmed_reads.join(genome_sizes)

    // correct reads
    (corrected_reads, lighter_output) = correct_reads(reads_and_genome_sizes)

    // find  read depths
    read_depths = lighter_output.map { pair_id, file -> find_average_depth(pair_id, file.text) }

    // downsample reads
    corrected_reads_and_read_depths = corrected_reads.join(read_depths)
    downsampled_reads = downsample_reads(corrected_reads_and_read_depths, params.depth_cutoff)
  emit:
    downsampled_reads

}